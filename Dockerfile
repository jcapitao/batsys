FROM ubuntu:16.04
RUN \
  apt-get -y update && \
  apt-get -y upgrade && \
  apt-get -y install software-properties-common && \
  apt-add-repository --yes --update ppa:ansible/ansible && \
  apt-get -y install ansible \
                     pkg-config \
                     openvpn \
                     unzip \
                     wget \
                     python3-setuptools \
                     python3-pip \
                     virtualenv \
		     sudo && \
  rm -rf /var/lib/apt/lists/*
RUN useradd -m -d /home/ansible -s /bin/bash -U ansible
RUN echo "ansible ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/ansible
USER ansible
