#!/bin/sh

sudo sed --in-place 's/^#\s*\(%wheel\s\+ALL=(ALL)\s\+NOPASSWD:\s\+ALL\)/\1/' /etc/sudoers
sudo dnf update -y
sudo dnf install -y ansible libselinux-python python-dnf
