.PHONY: install
install:
	ansible-playbook desktop.yml -i hosts --vault-password-file vault_password_file --ask-become-pass

.PHONY: install-without-packages
install-without-packages:
	ansible-playbook desktop.yml -i hosts --vault-password-file vault_password_file --ask-become-pass --skip-tags packages

.PHONY: test
test:
	ansible-playbook desktop.yml -i hosts --extra-vars "@host_vars/stub.yml" --ask-become-pass

.PHONY: create-vault
create-vault:
	ansible-vault encrypt_string --vault-password-file vault_password_file --name 'the_secret'
